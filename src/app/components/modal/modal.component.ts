import {Component, ElementRef, EventEmitter, HostListener, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Output() close = new EventEmitter();
  @ViewChild('modal') private modal: ElementRef | undefined;

  constructor(private elRef: ElementRef) { }

  ngOnInit(): void {
  }

  @HostListener('document:click', ['$event'])
  clickOut(event: Event): void {
    if (this.elRef && this.elRef.nativeElement.contains(event.target)) {
      event.stopPropagation();
      this.close.emit();
    }
  }

}

