import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-cookies-policy',
  templateUrl: './cookies-policy.component.html',
  styleUrls: ['./cookies-policy.component.css']
})
export class CookiesPolicyComponent implements OnInit {
  @Output() openEvent = new EventEmitter();
  @Output() closeEvent = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  openCookiesPolicy() {
    this.openEvent.emit();
  }

  closeCookiesPolicy() {
    this.closeEvent.emit();
  }
}
