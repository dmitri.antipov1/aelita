import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiesContentComponent } from './cookies-content.component';

describe('CookiesContentComponent', () => {
  let component: CookiesContentComponent;
  let fixture: ComponentFixture<CookiesContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CookiesContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CookiesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
