import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { GalleryModule } from 'ng-gallery';

import { AppComponent } from './app.component';
import { LogoComponent } from './icons/logo/logo.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LightComponent } from './icons/light/light.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import {RouterOutlet} from "@angular/router";
import { AppRoutingModule } from './app-routing.module';
import { AboutComponent } from './pages/about-page/about.component';
import { CookiesPolicyComponent } from './components/cookies-policy/cookies-policy.component';
import { ModalComponent } from './components/modal/modal.component';
import { AgreementComponent } from './components/agreement/agreement.component';
import { CookiesContentComponent } from './components/cookies-content/cookies-content.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { PolicyComponent } from './components/policy/policy.component';
import { SofaComponent } from './icons/sofa/sofa.component';
import { HandWithHeartComponent } from './icons/hand-with-heart/hand-with-heart.component';
import { PhoneComponent } from './icons/phone/phone.component';
import { MailComponent } from './icons/mail/mail.component';
import { LocationComponent } from './icons/location/location.component';
import { ClockComponent } from './icons/clock/clock.component';
import { PricePageComponent } from './pages/price-page/price-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { ReviewsPageComponent } from './pages/reviews-page/reviews-page.component';
import { MapPageComponent } from './pages/map-page/map-page.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LogoComponent,
    LightComponent,
    MainPageComponent,
    AboutComponent,
    CookiesPolicyComponent,
    ModalComponent,
    AgreementComponent,
    CookiesContentComponent,
    PolicyComponent,
    SofaComponent,
    HandWithHeartComponent,
    PhoneComponent,
    MailComponent,
    LocationComponent,
    ClockComponent,
    PricePageComponent,
    ContactPageComponent,
    TeamPageComponent,
    ReviewsPageComponent,
    MapPageComponent
  ],
  imports: [
    BrowserModule,
    GalleryModule,
    BrowserAnimationsModule,
    RouterOutlet,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
