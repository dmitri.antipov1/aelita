import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandWithHeartComponent } from './hand-with-heart.component';

describe('HandWithHeartComponent', () => {
  let component: HandWithHeartComponent;
  let fixture: ComponentFixture<HandWithHeartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HandWithHeartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HandWithHeartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
