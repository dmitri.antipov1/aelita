import { Component, OnInit } from '@angular/core';
import {GalleryItem, ImageItem} from "ng-gallery";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  images: GalleryItem[] = [];

  constructor() { }

  ngOnInit(): void {
    this.images = [
      new ImageItem({ src: 'assets/images/hospital/1.jpeg', thumb: 'assets/images/hospital/1.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/2.jpeg', thumb: 'assets/images/hospital/2.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/3.jpeg', thumb: 'assets/images/hospital/3.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/4.jpeg', thumb: 'assets/images/hospital/4.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/5.jpeg', thumb: 'assets/images/hospital/5.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/6.jpeg', thumb: 'assets/images/hospital/6.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/7.jpeg', thumb: 'assets/images/hospital/7.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/8.jpeg', thumb: 'assets/images/hospital/8.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/9.jpeg', thumb: 'assets/images/hospital/9.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/10.jpeg', thumb: 'assets/images/hospital/10.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/11.jpeg', thumb: 'assets/images/hospital/11.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/12.jpeg', thumb: 'assets/images/hospital/12.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/13.jpeg', thumb: 'assets/images/hospital/13.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/14.jpeg', thumb: 'assets/images/hospital/14.jpeg' }),
      new ImageItem({ src: 'assets/images/hospital/15.jpeg', thumb: 'assets/images/hospital/15.jpeg' }),
    ];
  }

}
