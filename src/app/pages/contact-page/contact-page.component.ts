import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ModalService} from "../../services/modal.service";
import {FormsService} from "../../services/forms.service";

export type ContactFormType = {
  name: string;
  email: string;
  message: string;
  checkInput: boolean;
}

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactPageComponent implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder,  private ms: ModalService, private fs: FormsService) {
    this.form = this.fb.group({
        name: ['', Validators.required],
        email: ['', Validators.required],
        message: ['', Validators.required],
        checkInput: [false, Validators.pattern('true')]
    })
  }

  ngOnInit(): void {
  }

  sendMessage() {
    if (this.form.valid) {
      this.fs.sendContactForm(this.form.value).subscribe((res) => console.log(res));
    }
  }

  openPolicy() {
    this.ms.openAgreementsModal();
  }
}
