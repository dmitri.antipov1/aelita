import {Component, OnInit} from '@angular/core';
import {SpecialistData, TeamService} from "../../services/team.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ModalService} from "../../services/modal.service";
import {FormsService} from "../../services/forms.service";

export type RegisterFormType = {
  doctor: string;
  name: string;
  phone: string;
  message: string;
  checkInput: boolean;
}

@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.css']
})
export class TeamPageComponent implements OnInit {
  public teamMembers: SpecialistData[] = this.ts.teamData;
  public teamNames: string[] = this.ts.specialistNames;
  public index = 0;
  public isModal = false;
  public form: FormGroup;

  constructor(private ts: TeamService, private fb: FormBuilder, private ms: ModalService, private fs: FormsService) {
    this.form = this.fb.group({
      doctor: ['', Validators.required],
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('[- +()0-9]+')]],
      message: ['', Validators.required],
      checkInput: [false, Validators.pattern('true')]
    })
  }

  ngOnInit(): void {
  }

  showSpecialist(person: SpecialistData): void {
    this.index = person.id;
    this.isModal = true;
  }

  closeModal() {
    this.index = 0;
    this.isModal = false;
  }

  openPolicy(): void {
    this.ms.openAgreementsModal();
  }

  sendForm(): void {
    if (this.form.valid) {
      this.fs.sendRegistrationForm(this.form.value).subscribe((res) => console.log(res));
    }
  }
}
