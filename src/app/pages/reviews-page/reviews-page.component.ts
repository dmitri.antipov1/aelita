import { Component, OnInit } from '@angular/core';
import {ModalService} from "../../services/modal.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FormsService} from "../../services/forms.service";

export type FeedBackFormType = {
  name: string;
  email: string;
  message: string;
  checkInput: boolean;
}

@Component({
  selector: 'app-reviews-page',
  templateUrl: './reviews-page.component.html',
  styleUrls: ['./reviews-page.component.css']
})
export class ReviewsPageComponent implements OnInit {
  form: FormGroup;

  constructor(private ms: ModalService, private fb: FormBuilder, private fs: FormsService) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
      checkInput: [false, Validators.pattern('true')],
    })
  }

  ngOnInit(): void {
  }

  openPolicy(): void {
    this.ms.openAgreementsModal();
  }

  sendForm(): void {
    if (this.form.valid) {
      this.fs.sendFeedBackForm(this.form.value).subscribe((res) => console.log(res));
    }
  }
}
