import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {ModalService} from "../../services/modal.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-about-page',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  @ViewChild('modalPopup') public modalPopup: ElementRef | undefined;
  isOpen$ = this.ms.noticeModal$;

  constructor(private ms: ModalService, private rm: Router) { }

  ngOnInit(): void {
  }

  openCookiesModal() {
    this.ms.openCookiesModal();
  }

  openAgreementsModal() {
    this.ms.openAgreementsModal();
  }

  openPersonalDataModal() {
    this.ms.openPersonalDataModal();
  }

  openNoticeModal() {
    this.ms.openNoticeModal();
  }


  goToContacts() {
    this.rm.navigate(['/contact']);
    this.ms.closeNoticeModal();
  }

  goToTeam() {
    this.rm.navigate(['/team']);
    this.ms.closeNoticeModal();
  }

  closeModal() {
    if (this.modalPopup?.nativeElement) {
      this.ms.closeNoticeModal();
    }
  }
}
