import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MainPageComponent} from "./pages/main-page/main-page.component";
import {AboutComponent} from "./pages/about-page/about.component";
import {PricePageComponent} from "./pages/price-page/price-page.component";
import {ContactPageComponent} from "./pages/contact-page/contact-page.component";
import {TeamPageComponent} from "./pages/team-page/team-page.component";
import {ReviewsPageComponent} from "./pages/reviews-page/reviews-page.component";
import {MapPageComponent} from "./pages/map-page/map-page.component";


const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: MainPageComponent, data: {animation: 'main'}, pathMatch: 'full' },
  { path: 'about', component: AboutComponent, data: {animation: 'about'}, pathMatch: 'full' },
  { path: 'price', component: PricePageComponent, data: {animation: 'price'}, pathMatch: 'full' },
  { path: 'team', component: TeamPageComponent, data: {animation: 'team'}, pathMatch: 'full' },
  { path: 'map', component: MapPageComponent, data: {animation: 'map'}, pathMatch: 'full' },
  { path: 'reviews', component: ReviewsPageComponent, data: {animation: 'reviews'}, pathMatch: 'full' },
  { path: 'contact', component: ContactPageComponent, data: {animation: 'contact'}, pathMatch: 'full' },
  {path: '404', redirectTo: 'main'},
  {path: '**', redirectTo: 'main'}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
