import {Component, OnInit} from '@angular/core';
import {ModalService} from "./services/modal.service";
import {CookiesService} from "./services/cookies.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  cookiesPolicy$ = this.ms.isCookiesOpen$;
  agreementsPolicy$ = this.ms.isAgreementsOpen$;
  personalDataPolicy$ = this.ms.isPersonalDataOpen$;
  cookiesOpen$ = this.cs.isOpen$;
  isOpenBurger = false;

  constructor(private ms: ModalService, private cs: CookiesService) {
  }

  closeCookiesPolicy(): void {
    this.ms.closeCookiesModal();
  }

  openCookiesPolicy(): void {
    this.ms.openCookiesModal();
  }

  closeCookiesBar(): void {
    this.cs.close();
  }

  openBurger(): void {
    this.isOpenBurger = !this.isOpenBurger;
  }

  closeAgreementsPolicy(): void {
    this.ms.closeAgreementsModal();
  }

  closePersonalData(): void {
    this.ms.closePersonalDataModal();
  }
}
