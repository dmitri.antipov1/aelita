import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {RegisterFormType} from "../pages/team-page/team-page.component";
import { ContactFormType } from '../pages/contact-page/contact-page.component';
import {FeedBackFormType} from "../pages/reviews-page/reviews-page.component";

@Injectable({
  providedIn: 'root'
})
export class FormsService {

  constructor(private http: HttpClient) { }

  sendRegistrationForm(body: RegisterFormType): Observable<any> {
    const formData = new FormData();
    formData.append('nameDoc', body.doctor);
    formData.append('nameUser', body.name);
    formData.append('phone', body.phone);
    formData.append('message', body.message);
    return this.http.post('https://forms.xn--80abckcp3bls.xn--p1ai/ingress/form/7aff8f99109a90adf37501a99b4da3d0', formData);
  }

  sendContactForm(body: ContactFormType): Observable<any> {
    const formData = new FormData();
    formData.append('name', body.name);
    formData.append('mail', body.email);
    formData.append('message', body.message);
    return this.http.post('https://forms.xn--80abckcp3bls.xn--p1ai/ingress/form/b8dc187e7d5ddeb7c281d99506841ad6', formData);
  }

  sendFeedBackForm(body: FeedBackFormType): Observable<any> {
    const formData = new FormData();
    formData.append('name', body.name);
    formData.append('mail', body.email);
    formData.append('opinionMessage', body.message);
    return this.http.post('https://forms.xn--80abckcp3bls.xn--p1ai/ingress/form/2f55d0f0b629a11940b1e7a417f27954', formData);
  }
}
