import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CookiesService {
  isOpen$ = new BehaviorSubject<boolean>(true);

  constructor() {
    if (localStorage.getItem('cookies')) {
      this.isOpen$.next(false);
    }
  }

  close(): void {
    if (localStorage.getItem('cookies')) {
      this.isOpen$.next(false);
    }
    localStorage.setItem('cookies', 'true');
    this.isOpen$.next(false);
  }


}
