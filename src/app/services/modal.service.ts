import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  isCookiesOpen$ = new BehaviorSubject<boolean>(false);
  isAgreementsOpen$ = new BehaviorSubject<boolean>(false);
  isPersonalDataOpen$ = new BehaviorSubject<boolean>(false);
  noticeModal$ = new BehaviorSubject<boolean>(false);

  constructor() { }

  openCookiesModal(): void {
    this.isCookiesOpen$.next(true);
  }

  closeCookiesModal(): void {
    this.isCookiesOpen$.next(false);
  }

  openAgreementsModal(): void {
    this.isAgreementsOpen$.next(true);
  }

  closeAgreementsModal(): void {
    this.isAgreementsOpen$.next(false);
  }

  openPersonalDataModal(): void {
    this.isPersonalDataOpen$.next(true);
  }

  closePersonalDataModal(): void {
    this.isPersonalDataOpen$.next(false);
  }

  closeNoticeModal(): void {
    this.noticeModal$.next(false);
  }

  openNoticeModal(): void {
    this.noticeModal$.next(true);
  }
}
